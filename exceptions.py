class NotEnoughTermsError(Exception):
    def __init__(self, min=1, got=0):
        self.message = "At least "+str(min)+" search term must be provided, received "+str(got)+"."
        super().__init__(self.message)