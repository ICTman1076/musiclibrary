import os, taglib

def scanDirectory(path):
    songs = []
    """Scan a directory recursively for files. If one is found, a load is attempted"""
    if os.path.isdir(path):
        for thingName in os.listdir(path):
            thingPath = "{}/{}".format(path, thingName)
            if os.path.isdir(thingPath):
                dataList = scanDirectory(thingPath)
                for song in dataList:
                    songs.append(song)
            else:
                data = loadFile(thingPath)
                if data != False:
                    songs.append(data)
    return songs


def isAcceptableFile(name):
    """Check if a filename has a file extension that matches accepted audio file types."""
    mp3 = name.endswith(".mp3")
    ogg = name.endswith(".ogg")
    flac = name.endswith(".flac")
    wav = name.endswith(".wav")
    aiff = name.endswith(".aiff")
    mp4 = name.endswith(".mp4")
    return mp3 or ogg or flac or wav or aiff or mp4

def loadFile(path):
    """Check a file to see if it is a song. If it is, return its data."""
    if os.path.isfile(path) and isAcceptableFile(path):
        song = taglib.File(path)
        tags = song.tags

        # find and do basic parsing to the artists
        artistTags = tags.get("ARTIST", tags.get("ARTISTS", ["Unknown artist"]))
        artists = []
        for artistTag in artistTags:
            artists_1 = artistTag.split("&")
            for i in artists_1:
                content = i.strip()
                artists_2 = content.split(",")
                for j in artists_2:
                    artists.append(j.strip())
        
        # find the song name
        songName = tags.get("TITLE", ["Unknown title"])[0]

        # find the album info
        albumArtistTags = tags.get("ALBUMARTIST", tags.get("ALBUMARTISTS", ["Unknown artist"]))
        albumArtists = []
        for artistTag in albumArtistTags:
            artists_1 = artistTag.split("&")
            for i in artists_1:
                content = i.strip()
                artists_2 = content.split(",")
                for j in artists_2:
                    albumArtists.append(j.strip())
        album = {
            "name": tags.get("ALBUM",["Unknown album"])[0],
            "artists": albumArtists 
        }

        # find genres
        genreTags = tags.get("GENRE",["Unknown genre"])
        genres = []
        for tag in genreTags:
            genresUnfixed = tag.split(",")
            for genre in genresUnfixed:
                genres.append(genre.strip())

        year = tags.get("YEAR",tags.get("ORIGINALYEAR", ["Unknown"]))[0]
        discNumber = tags.get("DISC", tags.get("DISCNUMBER", ["1"]))[0]
        discNumber = discNumber.split("/")[0]
        trackNumber = tags.get("TRACKNUMBER", ["0"])[0]
        trackNumber = trackNumber.split("/")[0]

        return {
            "name": songName,
            "artists": artists,
            "album": album,
            "genres": genres,
            "year": year,
            "discNumber": int(discNumber),
            "trackNumber": int(trackNumber),
            "originalTags": tags
        }
    else:
        return False