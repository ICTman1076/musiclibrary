from .scan import scanDirectory, loadFile
from .db import objects, createEngine, makeSession, idGen
from sqlalchemy.orm.exc import NoResultFound
import os.path

class dbCreator:
    """
    Database creator object. Can create or update existing databases.
    """
    def __init__(self, path="sqlite:///data/library.db"):
        """
        Initialise the dbCreator object with the database at path. If a database there
        doesn't exist, or it's not set up for a library, it will create one.

        path: str - path to database. Uses SQLAlchemy structure. 
        """
        self.engine = createEngine(path)
        objects.Base.metadata.create_all(self.engine)

    def createLibrary(self, paths):
        """
        Takes a list of paths, finds every song, and then creates each song in the database.

        paths: list - list of file paths to check.
        """
        fullSongs = []
        for path in paths:
            songs = scanDirectory(path)
            for s in songs:
                fullSongs.append(s)
        conn = makeSession()
        for song in fullSongs:
            self.createSong(song, conn)
        
    def createSong(self, song, conn=None):
        """
        Creates a song in the database, linked to an artist or an album. If an artist or album
        doesn't exist it will be made.

        song: dict - dictionary containing song data
        conn: An SQLAlchemy session to the database. If one isn't supplied it will make one.
        """
        if conn == None: conn = makeSession()
        artists = []
        artistIds = []
        for artist in song["artists"]:
            artistdb = self.getArtist(artist, conn)
            artists.append(artistdb)
            artistIds.append(artistdb.id)
        genres = []
        for genre in song["genres"]:
            genres.append(self.getGenre(genre, conn))
        res = conn.query(objects.Song).filter(objects.Song.name==song["name"])
        try:
            songs = res.all()
            currentSong = ""
            currentSongPass = False
            for songdb in songs:
                currentSongPass = True
                currentSong = songdb
                s = objects.song_to_artist.select().where(objects.song_to_artist.c.songId==songdb.id)
                res2 = self.engine.connect().execute(s)
                for artistdb in res2:
                    if not artistdb.artistId in artistIds:
                        currentSongPass = False
                if currentSongPass:
                    break
            if not currentSongPass:
                raise NoResultFound("all results invalid")
            return currentSong
        except NoResultFound:
            songId = idGen()
            album = self.getAlbum(song["album"], conn)
            song = objects.Song(id=songId, name=song["name"], year=song["year"], discNumber=song["discNumber"],
                                trackNumber=song["trackNumber"], albumId=album.id )
            conn.add(song)
            for artist in artists:
                s = objects.song_to_artist.insert().values(songId=songId, artistId=artist.id)
                self.engine.connect().execute(s)
            for genre in genres:
                s = objects.song_to_genre.insert().values(songId=songId, genreId=genre.id)
                self.engine.connect().execute(s)
            conn.commit()

            return song

    def getArtist(self, artistName, conn=None):
        if conn == None: conn = makeSession()
        res = conn.query(objects.Artist).filter(objects.Artist.name == artistName)
        try:
            artist = res.one()
        except NoResultFound:
            artist = objects.Artist(id=idGen(), name=artistName)
            conn.add(artist)
            conn.commit()
        return artist

    def getGenre(self, genreName, conn=None):
        if conn == None: conn = makeSession()
        res = conn.query(objects.Genre).filter(objects.Genre.name == genreName)
        try:
            genre = res.one()
        except NoResultFound:
            genre = objects.Genre(id=idGen(), name=genreName)
            conn.add(genre)
            conn.commit()
        return genre

    def getAlbum(self, albumData, conn_ignored=None):
        conn = makeSession()
        res = conn.query(objects.Album).filter(objects.Album.name == albumData["name"])
        artistIds = []
        for artistData in albumData["artists"]:
            artistIds.append(self.getArtist(artistData, conn).id)
        try:
            albums = res.all()
            currentSong = ""
            currentSongPass = False
            for album in albums:
                currentSongPass = True
                currentSong = album
                s = objects.artist_to_album.select().where(objects.artist_to_album.c.albumId==album.id)
                res2 = self.engine.connect().execute(s)
                for artistdb in res2:
                    if not artistdb.artistId in artistIds:
                        currentSongPass = False
                if currentSongPass:
                    break
            if not currentSongPass:
                raise NoResultFound("all results invalid")
            return currentSong
        except NoResultFound:
            albumId = idGen()
            album = objects.Album(id=albumId, name=albumData["name"])
            conn.add(album)
            for artist in artistIds:
                s = objects.artist_to_album.insert().values(albumId=albumId, artistId=artist)
                self.engine.connect().execute(s)
            conn.commit()
            return album

