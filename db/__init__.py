from .objects import Objects
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from time import time
import random

engine = None
def createEngine(path="sqlite:///data/library.db"):
    global engine
    engine = create_engine(path)
    return engine
    
def makeSession():
    sess = sessionmaker(bind = engine)
    return sess()

def idGen():
    timePortion = int(time()*1000)
    randomPortion = random.randint(0,99)
    return timePortion*100+randomPortion

def createDB(path="sqlite:///data/library.db"):
    engine = create_engine(path)
    Objects.Base.metadata.create_all(engine)