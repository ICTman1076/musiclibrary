from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
Base = declarative_base()

song_to_artist = Table('song_artist_assoc', Base.metadata,
    Column('songId', Integer, ForeignKey('songs.id')),
    Column('artistId', Integer, ForeignKey('artists.id'))
)


song_to_genre = Table('song_genre_assoc', Base.metadata,
    Column('songId', Integer, ForeignKey('songs.id')),
    Column('genreId', Integer, ForeignKey('genres.id'))
)


artist_to_album = Table('album_artist_assoc', Base.metadata,
    Column('albumId', Integer, ForeignKey('albums.id')),
    Column('artistId', Integer, ForeignKey('artists.id'))
)

class Song(Base):
    __tablename__ = 'songs'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    artists = relationship(
        "Artist",
        secondary=song_to_artist,
        back_populates="songs")
    genres = relationship(
        "Genre",
        secondary=song_to_genre,
        back_populates="songs")
    albumId = Column(Integer, ForeignKey("albums.id"))
    album = relationship("Album", back_populates="songs")
    year = Column(String)
    trackNumber = Column(Integer)
    discNumber = Column(Integer)

class Artist(Base):
    __tablename__ = 'artists'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    songs = relationship(
        "Song",
        secondary=song_to_artist,
        back_populates="artists")
    albums = relationship(
        "Album",
        secondary=artist_to_album,
        back_populates="artists")

class Genre(Base):
    __tablename__ = 'genres'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    songs = relationship(
        "Song",
        secondary=song_to_genre,
        back_populates="genres")

class Album(Base):
    __tablename__ = 'albums'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    artists = relationship(
        "Artist",
        secondary=artist_to_album,
        back_populates="albums")
    songs = relationship(
        "Song",
        back_populates="album")

class Objects:
    Song = Song
    Artist = Artist
    Genre = Genre
    Album = Album
    Base = Base
    song_to_artist = song_to_artist
    song_to_genre = song_to_genre
    artist_to_album = artist_to_album