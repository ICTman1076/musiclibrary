from .create import *
from .scan import *
from .db import objects, createEngine, makeSession
from .exceptions import *
from sqlalchemy.orm.exc import NoResultFound

class Library:
    """
    Library object. Used to search songs.
    """
    def __init__(self, dbpath="sqlite:///data/library.db"):
        """
        Initialise the object, with a path to the library database.

        dbpath: str - path to the database. Uses SQLAlchemy structure.
        """
        self.engine = createEngine(dbpath)

    def searchSong(self, name=None, artists=None, album=None, genres=None):
        """
        Finds song(s) by name, artist and album if one is provided.

        Returns an SQLAlchemy query object (which can act like an array of db.objects.Song)

        name: str - name of the song
        artist: str/db.objects.Artist/list - song's artist(s). If an artist object is given it
                will match the artist exactly. If a string is given it will search
                for the artist with searchArtist() and match any similar artist name.
        album: str/db.objects.Album - song's album. If an album object is given it
                will match the album exactly. If a string is given it will search
                for the album name with searchAlbum() and match any similar album name.
        genres: str/db.objects.Genre/list - song's genre(s). If a genre object is given it
                will match the genre exactly. If a string is given it will search
                for the genre with searchGenre() and match any similar genre.

        No single argument is required, but you must provide at least one.
        """
        sess = makeSession()
        query = sess.query(objects.Song)
        filters = []
        conn = self.engine.connect()
        if artists != None:
            acceptableSongIds = []
            if not isinstance(artists, list):
                artists = [artists]
            artistObjects = []
            for artist in artists:
                if isinstance(artist, str):
                    artistObj = self.searchArtist(artist)
                else:
                    artistObj = [artist]
                for i in artistObj:
                    s = objects.song_to_artist.select().where(objects.song_to_artist.c.artistId==i.id)
                    res2 = conn.execute(s)
                    for row in res2:
                        acceptableSongIds.append(row[0])
            filters.append(objects.Song.id.in_(acceptableSongIds))
        if album != None:
            acceptableAlbumIds = []
            if isinstance(album, str):
                albumObj = self.searchAlbum(album)
            else:
                albumObj = [album]
            for i in albumObj:
                acceptableAlbumIds.append(i.id)
            filters.append(objects.Song.albumId.in_(acceptableAlbumIds))
        if genres != None:
            acceptableSongIdsG = []
            if not isinstance(genres, list):
                genres = [genres]
            artistObjects = []
            for genre in genres:
                if isinstance(genre, str):
                    genreObj = self.searchGenres(genre)
                else:
                    genreObj = [genre]
                for i in genreObj:
                    s = objects.song_to_genre.select().where(objects.song_to_genre.c.genreId==i.id)
                    res2 = conn.execute(s)
                    for row in res2:
                        acceptableSongIdsG.append(row[0])
            filters.append(objects.Song.id.in_(acceptableSongIdsG))
        if name != None:
            filters.append(objects.Song.name.like("%"+name+"%"))
        if filters == []:
            raise NotEnoughTermsError()
        filteredQuery = query.filter(*filters)
        return filteredQuery
    
    def searchArtist(self, name):
        """
        Finds artist(s) by name.
        
        Returns a SQLAlchemy Query object (which can act like an array of db.objects.Artist)

        name: str - name of artist. Required
        """
        sess = makeSession()
        query = sess.query(objects.Artist).filter(objects.Artist.name.like("%"+name+"%"))
        return query

    def searchGenres(self, name):
        """
        Finds genre(s) by name.
        
        Returns a SQLAlchemy Query object (which can act like an array of db.objects.Genre)

        name: str - name of genre. Required
        """
        sess = makeSession()
        query = sess.query(objects.Genre).filter(objects.Genre.name.like("%"+name+"%"))
        return query

    def searchAlbum(self, name=None, artists=None):
        """
        Finds albums(s) by name, artist and album if one is provided.

        Returns an SQLAlchemy query object (which can act like an array of db.objects.Album)

        name: str - name of the album
        artist: str/db.objects.Artist/list - album's artist(s). If an artist object is given it
                will match the artist exactly. If a string is given it will search
                for the artist with searchArtist() and match any similar artist name.

        No single argument is required, but you must provide at least one.
        """
        sess = makeSession()
        query = sess.query(objects.Album)
        filters = []
        conn = self.engine.connect()
        if artists != None:
            acceptableAlbumIds = []
            if not isinstance(artists, list):
                artists = [artists]
            artistObjects = []
            for artist in artists:
                if isinstance(artist, str):
                    artistObj = self.searchArtist(artist)
                else:
                    artistObj = [artist]
                for i in artistObj:
                    s = objects.artist_to_album.select().where(objects.artist_to_album.c.artistId==i.id)
                    res2 = conn.execute(s)
                    for row in res2:
                        acceptableAlbumIds.append(row[0])
                        
            filters.append(objects.Album.id.in_(acceptableAlbumIds))
        if name != None:
            filters.append(objects.Album.name.like("%"+name+"%"))
        if filters == []:
            raise NotEnoughTermsError()
        filteredQuery = query.filter(*filters)
        return filteredQuery

    def getSong(self, id):
        """
        Get a song object using its ID.

        id: int - the song ID. Required
        """
        sess = makeSession()
        query = sess.query(objects.Song).filter(objects.Song.id == id)
        try:
            return query.one()
        except NoResultFound:
            return False

    def getAlbum(self, id):
        """
        Get an album object using its ID.

        id: int - the album ID. Required
        """
        sess = makeSession()
        query = sess.query(objects.Album).filter(objects.Album.id == id)
        try:
            return query.one()
        except NoResultFound:
            return False

    def getArtist(self, id):
        """
        Get an artist object using its ID.

        id: int - the artist ID. Required
        """
        sess = makeSession()
        query = sess.query(objects.Artist).filter(objects.Artist.id == id)
        try:
            return query.one()
        except NoResultFound:
            return False